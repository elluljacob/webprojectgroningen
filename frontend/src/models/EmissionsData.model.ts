export default interface EmissionsData {
    country: string;
    ISOCode: string;
    year: number;
    CO2: number;
    methane: number;
    nitrousOxide: number;
    totalGHG: number;
}