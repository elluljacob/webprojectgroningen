export default interface CountryYearlyData {
    country: string;
    ISOCode: string;
    year: number;
    GDP: number;
    population: number;
}
  