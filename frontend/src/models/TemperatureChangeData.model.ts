export default interface TemperatureChangeData {
    country: string;
    year: number;
    temperatureChangeFromCH4: number;
    temperatureChangeFromN2O: number;
    temperatureChangeFromCO2: number;
    shareOfTemperatureChangeFromGHG: number;
}