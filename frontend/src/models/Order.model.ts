export enum EmissionsColumns {
    Year = 'Year',
    CO2 = 'CO2',
    Methane = 'Methane',
    NitrousOxide = 'NitrousOxide',
    TotalGHG = 'TotalGHG',
}

export enum EnergyColumns {
    Year = 'Year',
    EnergyPerCapita = 'EnergyPerCapita',
    EnergyPerGDP = 'EnergyPerGDP',
}

export enum TemperatureChangeDataColumns {
    Year = 'Year',
    shareOfTemperatureChangeFromGHG = 'share-of-temperature-change-from-ghg',
    temperatureChangeFromCH4 = 'temperature-change-from-ch4',
    temperatureChangeFromCO2 = 'temperature-change-from-co2',
    temperatureChangeFromGHG = 'temperature-change-from-ghg',
    temperatureChangeFromN2O = 'temperature-change-from-n2o',
}

export enum ClimateContributionColumns {
    Year = "Year",
    CO2 = "CO2",
    Methane = "Methane",
    NitrousOxide = "NitrousOxide",
    TotalGHG = "TotalGHG"
}

export enum OrderDir {
    Asc = 'Asc',
    Desc = 'Desc',
}

export default interface OrderParams {
    by?: EmissionsColumns | EnergyColumns | TemperatureChangeDataColumns | ClimateContributionColumns;
    direction?: OrderDir;
}
