export default interface Filter {
    year?: number;
    limit?: number;
    offset?: number;
}
