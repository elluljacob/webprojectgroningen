export default interface EnergyData extends Array<{
    country: string;
    energyPerCapita: number;
    energyPerGDP: number;
    population?: number;
}> { }// This was done differently to handle pagination
