interface ErrorDisplayProps {
    message: string;
}
  
const ErrorDisplay: React.FC<ErrorDisplayProps> = ({ message }) => (
    <div style={{ color: 'red' }}>Error: {message}</div>
);
  
export default ErrorDisplay;