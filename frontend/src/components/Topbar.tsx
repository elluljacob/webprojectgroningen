import { DarkMode, LightMode } from "@mui/icons-material";
import { AppBar, Box, IconButton, Link, Toolbar, Typography } from "@mui/material";
import { useContext } from "react";
import { ColorModeContext } from "../context/ColourModeContext";
// Also taken from the tutorials

/**
 * These are the props for the TopbarLink component.
 * 
 * @property href - The href for the link.
 * @property text - The text to display for the link.
 * @property external - Whether or not the link is external.
 */
interface TopbarLinkProps {
    href: string;
    text: string;
    external?: boolean;
}

/**
 * This component is a link for the topbar.
 * 
 * @param props - The props for the component. 
 * @returns - The component
 */
function TopbarLink(props: TopbarLinkProps) {
    let { href, text, external } = {
        external: false,
        ...props
    };

    return <Box mx={2}>
        <Link href={href} underline="none" color="inherit"
            target={external ? "_blank" : "_self"}
        >
            <Typography>
                {text}
            </Typography>
        </Link>
    </Box>;
}

/**
 * This component is the topbar.
 * 
 * @returns - The component
 */
export default function Topbar() {
    const { toggleColorMode, currentTheme } = useContext(ColorModeContext);

    return <Box flexGrow={1}>
        <AppBar position="static">
            <Toolbar>
                <TopbarLink href="/" text="Home" />
                <TopbarLink href="https://rug.nl/" text="RuG Website" external />
                <TopbarLink href="https://brightspace.rug.nl/" text="Brightspace" external />
                <Box flexGrow={1} />
                <IconButton onClick={toggleColorMode} >
                    {currentTheme === "light" ?
                        <DarkMode /> :
                        <LightMode />}
                </IconButton>
            </Toolbar>
        </AppBar>
    </Box>;
}

