import React, { useState } from 'react';
import { useEnergyData } from '../hooks/useEnergyData';
import LoadingIndicator from './LoadingIndicator';
import ErrorDisplay from './ErrorDisplay';
import { OrderDir } from '../models/Order.model';

const EnergyDataViewer: React.FC = () => {
  const [year, setYear] = useState<number>(new Date().getFullYear());
  const [batchSize, setBatchSize] = useState<number>(10);
  const [offset, setOffset] = useState<number>(0);

  const { data, loading, error, fetchData } = useEnergyData();

  const handleFetchData = () => {
    fetchData(year, batchSize, offset, OrderDir.Asc);
  };

  const handleYearChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setYear(parseInt(e.target.value) || new Date().getFullYear());
  };

  const handleBatchSizeChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setBatchSize(parseInt(e.target.value));
    setOffset(0); // offset -> 0 when batch size changes
  };

  const handlePreviousPage = () => {
    const newOffset = Math.max(0, offset - batchSize);
    if (offset !== newOffset) {
      setOffset(newOffset);
      fetchData(year, batchSize, newOffset, OrderDir.Asc); // Fetch new data set
    }
  };

  const handleNextPage = () => {
    if (data && data.length === batchSize) {
      const newOffset = offset + batchSize;
      setOffset(newOffset);
      fetchData(year, batchSize, newOffset, OrderDir.Asc); // Fetch new data set
    }
  };

  if (loading) return <LoadingIndicator />;
  if (error) return <ErrorDisplay message={error.message} />;

  return (
    <div>
      <h3>Energy Data for the Year</h3>
      <p>The input fields below are "year", "batchSize" and "offset" in that order</p>
      <input type="number" value={year} onChange={handleYearChange} />

      <select value={batchSize} onChange={handleBatchSizeChange}>
        <option value={10}>10</option>
        <option value={20}>20</option>
        <option value={50}>50</option>
        <option value={100}>100</option>
      </select>
      <input
        type="number"
        value={offset}
        onChange={(e) => setOffset(parseInt(e.target.value))}
        placeholder="Offset"
      />

      {data && data.map((countryData, index) => (
        <ul key={index}>
          <li>Country: {countryData.country}</li>
          <li>Energy Per Capita: {countryData.energyPerCapita}</li>
          <li>Energy Per GDP: {countryData.energyPerGDP}</li>
        </ul>
      ))}

      <button onClick={handleFetchData}>Fetch Data</button>

      <div>
        <button onClick={handlePreviousPage} disabled={offset === 0}>Previous</button>
        <button onClick={handleNextPage} disabled={!data || data.length < batchSize}>Next</button>
      </div>
    </div>
  );
};

export default EnergyDataViewer;
