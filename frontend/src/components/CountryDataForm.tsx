import React, { useState } from 'react';
import useCountryYearlyData from '../hooks/useCountryYearlyData';

interface Props {
    onSubmission: (data: any) => void;
}

const CountryDataForm: React.FC<Props> = ({ onSubmission }) => {
    const [countryIsoCode, setCountryIsoCode] = useState('');
    const [year, setYear] = useState<number>();
    const [population, setPopulation] = useState<number>();
    const [GDP, setGDP] = useState<number>();

    const {
        data,
        fetchData,
        addData,
        updateData,
        removePopulationData,

        removeGDPData
    } = useCountryYearlyData(countryIsoCode, year || 0);

    const isDataPresent = data && data.population && data.GDP;
    const handleSubmit = async () => {
        await fetchData();
        if (isDataPresent) {
            await updateData(population || 0, GDP || 0);
        } else {
            await addData(population || 0, GDP || 0);
        }
        onSubmission({ countryIsoCode, year, population, GDP });
    };

    const handleDeletePopulation = async () => {
        if (year) {
            await removePopulationData();
            onSubmission({ countryIsoCode, year, population: undefined, GDP });
        }
    };
    const handleDeleteGDP = async () => {
        if (year) {
            await removeGDPData();
            onSubmission({ countryIsoCode, year, population, GDP: undefined });
        }
    };
    return (
        <div>
            <input type="text" value={countryIsoCode} onChange={(e) => setCountryIsoCode(e.target.value)} placeholder="Country ISO Code" />
            <input type="number" value={year} onChange={(e) => setYear(parseInt(e.target.value))} placeholder="Year" />
            <input type="number" value={population} onChange={(e) => setPopulation(parseInt(e.target.value))} placeholder="Population" />
            <input type="number" value={GDP} onChange={(e) => setGDP(parseInt(e.target.value))} placeholder="GDP" />
            <button onClick={handleSubmit}>Submit</button>
            <button onClick={handleDeletePopulation}>Delete Population Data</button>
            <button onClick={handleDeleteGDP}>Delete GDP Data</button>
        </div>
    );
};

export default CountryDataForm;