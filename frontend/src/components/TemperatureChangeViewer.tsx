import React from 'react';
import LoadingIndicator from './LoadingIndicator';
import ErrorDisplay from './ErrorDisplay';
import TemperatureChangeData from '../models/TemperatureChangeData.model';

interface Props {
  continentName: string;
  year?: number;
  data?: TemperatureChangeData[];
  loading: boolean;
  error: Error | null;
}

const TemperatureChangeViewer: React.FC<Props> = ({ continentName, data, loading, error }) => {
  if (loading) return <LoadingIndicator />;
  if (error) return <ErrorDisplay message={error.message} />;

  return (
    <div>
      <h3>Temperature Change Data for {continentName}</h3>
      {data && data.map((item, index) => (
        <ul key={index}>
          <li>Year: {item.year}</li>
          <li>CH4: {item.temperatureChangeFromCH4}</li>
          <li>N2O: {item.temperatureChangeFromN2O}</li>
          <li>CO2: {item.temperatureChangeFromCO2}</li>
          <li>Share of GHG: {item.shareOfTemperatureChangeFromGHG}</li>
        </ul>
      ))}
    </div>
  );
};
export default TemperatureChangeViewer;
