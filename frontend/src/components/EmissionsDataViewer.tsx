import React from 'react';
import LoadingIndicator from './LoadingIndicator';
import ErrorDisplay from './ErrorDisplay';
import EmissionsData from '../models/EmissionsData.model';

interface Props {
  countryIsoCode: string;
  year: number;
  data?: EmissionsData[];
  loading: boolean;
  error: Error | null;
}

const EmissionsDataViewer: React.FC<Props> = ({ countryIsoCode, year, data, loading, error }) => {
  if (loading) return <LoadingIndicator />;
  if (error) return <ErrorDisplay message={error.message} />;

  return (
    <div>
      <h3>Emissions Data for {countryIsoCode} in {year
      }</h3>
      {loading && <LoadingIndicator />}
      {data && data.map((emission, index) => (
        <div key={index}>
          <h4>Record {index + 1}</h4>
          <ul>
            <li>CO2: {emission.CO2}</li>
            <li>Methane: {emission.methane}</li>
            <li>Nitrous Oxide: {emission.nitrousOxide}</li>
            <li>Total GHG: {emission.totalGHG}</li>
          </ul>
        </div>
      ))}
    </div>
  );
};

export default EmissionsDataViewer;