import React from 'react';
import LoadingIndicator from './LoadingIndicator';
import ErrorDisplay from './ErrorDisplay';
import ClimateContributionData from '../models/ClimateContributionData.model';

interface Props {
  data: ClimateContributionData[] | undefined;
  loading: boolean;
  error: Error | null;
  onFetchData: () => void;
}

const ClimateContributionViewer: React.FC<Props> = ({ data, loading, error, onFetchData }) => {
  return (
    <div>
      <h3>Climate Contribution Data</h3>
      <button onClick={onFetchData}>Fetch Data</button>
      {loading && <LoadingIndicator />}
      {error && <ErrorDisplay message={error.message} />}
      {data && (
        <ul>
          {data.map((entry, index) => (
            <li key={index}>
              <p>Country: {entry.country}</p>
              <p>Year: {entry.year}</p>
              <p>Temperature Change from CH4: {entry.temperatureChangeFromCH4}</p>
              <p>Temperature Change from N2O: {entry.temperatureChangeFromN2O}</p>
              <p>Temperature Change from CO2: {entry.temperatureChangeFromCO2}</p>
              <p>Share of Temperature Change from GHG: {entry.shareOfTemperatureChangeFromGHG}</p>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};


export default ClimateContributionViewer;
