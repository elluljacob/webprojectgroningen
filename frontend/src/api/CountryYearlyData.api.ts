import axios from "axios";

/**
 * 
 * @param backend backend url
 * @param countryIsoCode 
 * @param year 
 * @returns Data for a country for a given year
 */

export async function getPopulation(backend: string, countryIsoCode: string, year: number): Promise<number> {
    const response = await axios.get(`${backend}/countries/population`, {
        params: { isoOrCountry: countryIsoCode, year }
    });
    return response.data;
}
export async function getGDP(backend: string, countryIsoCode: string, year: number): Promise<number> {
    const response = await axios.get(`${backend}/countries/gdp`, {
        params: { isoOrCountry: countryIsoCode, year }
    });
    return response.data;
}
/**
 * 
 * @param backend backend url
 * @param countryIsoCode 
 * @param year 
 * @param population new population value
 * @returns Data for a country for a given year
 */
export async function addPopulation(backend: string, countryIsoCode: string, year: number, population: number): Promise<number> {
    const response = await axios.post(`${backend}/countries/population`, { population },
        {
            params: { isoOrCountry: countryIsoCode, year }
        });
    return response.data;
}


/**
 * 
 * @param backend backend url
 * @param countryIsoCode 
 * @param year 
 * @param population new population value
 * @returns Data for a country for a given year
 */
export async function updatePopulation(backend: string, countryIsoCode: string, year: number, population: number): Promise<number> {
    const response = await axios.patch(`${backend}/countries/population`, {
        population
    }, {
        params: { isoOrCountry: countryIsoCode, year }
    });
    return response.data;
}

/**
 * 
 * @param backend backend url
 * @param countryIsoCode 
 * @param year 
 * @returns 
 */
export async function deletePopulation(backend: string, countryIsoCode: string, year: number) {
    const response = await axios.delete(`${backend}/countries/population`, {
        params: { isoOrCountry: countryIsoCode, year }
    });
    return response.data;
}

/**
 * 
 * @param backend backend url
 * @param countryIsoCode 
 * @param year 
 * @param gdp new GDP value
 * @returns Data for a country for a given year
 */
export async function addGDP(backend: string, countryIsoCode: string, year: number, gdp: number): Promise<number> {
    const response = await axios.post(`${backend}/countries/gdp`, {
        gdp
    }, {
        params: { isoOrCountry: countryIsoCode, year }
    });
    return response.data;
}

/**
 * 
 * @param backend backend url
 * @param countryIsoCode 
 * @param year 
 * @param gdp new GDP value
 * @returns Data for a country for a given year
 */
export async function updateGDP(backend: string, countryIsoCode: string, year: number, gdp: number): Promise<number> {
    const response = await axios.patch(`${backend}/countries/gdp`, {
        gdp
    }, {
        params: { isoOrCountry: countryIsoCode, year }
    });
    return response.data;
}

/**
 * 
 * @param backend backend url
 * @param countryIsoCode 
 * @param year 
 * @returns 
*/
export async function deleteGDP(backend: string, countryIsoCode: string, year: number) {
    const response = await axios.delete(`${backend}/countries/gdp`, {
        params:
            { isoOrCountry: countryIsoCode, year }
    });
    return response.data;
}
