import axios from 'axios';
import EmissionsData from '../models/EmissionsData.model';
import Filter from '../models/FilterParams.model';
import OrderParams from '../models/Order.model';

export async function getCountryEmissions(
    backend: string,
    countryIsoCode: string,
    filterParams: Filter,
    orderParams: OrderParams,
    limit: number = 1,
    offset: number = 0
): Promise<EmissionsData[]> {
    console.log("Sending API request... getCountryEmissions");
    const response = await axios.get(`${backend}/countries/emissions`, {
        params: {
            isoOrCountry: countryIsoCode,
            ...filterParams,
            ...orderParams,
            'paging.limit': limit,
            'paging.offset': offset,
        }
    });
    console.log("returned data: ", response);
    return response.data;
}
