import axios from 'axios';
import TemperatureChangeData from '../models/TemperatureChangeData.model';
import Filter from '../models/FilterParams.model';
import OrderParams from '../models/Order.model';

export async function getTemperatureChangeData(
    backend: string,
    continentName: string,
    year: number | undefined,
    limit: number | undefined,
    filterParams: Filter,
    orderParams: OrderParams
): Promise<TemperatureChangeData[]> {
    console.log("Sending API request... getTempChangeData");
    const params = {
        continent: continentName,
        year: year,
        limit: limit,
        ...filterParams,
        ...orderParams
    };
    const response = await axios.get(`${backend}/continents/temperature-changes`, { params });
    console.log("returned data: ", response);
    return response.data;
}
