import axios from 'axios';
import EnergyData from '../models/EnergyData.model';
import OrderParams from '../models/Order.model';
import Filter from '../models/FilterParams.model';

export async function getEnergyData(backend: string, year: number, filterParams: Filter, orderParams: OrderParams): Promise<EnergyData> {
    console.log("Sending API request... getEnergyData");
    const response = await axios.get(`${backend}/energy/${year}`, { params: { ...filterParams, ...orderParams } })
    console.log("returned data: ", response);
    return response.data
}

