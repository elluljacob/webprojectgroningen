import axios from 'axios';
import ClimateContribution from '../models/ClimateContributionData.model';
import OrderParams from '../models/Order.model';
import Filter from '../models/FilterParams.model';

export async function getClimateContribution(backend: string, filterParams: Filter, orderParams: OrderParams): Promise<ClimateContribution[]> {
    console.log("Sending API request... getClimateContribution");
    const response = await axios.get(`${backend}/climate-contribution/countries`, { params: { ...filterParams, ...orderParams } })
    console.log("returned data: ", response);
    return response.data
}