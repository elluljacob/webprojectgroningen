import { BrowserRouter, Routes, Route } from "react-router-dom"
import { BackendProvider } from "./context/BackendProvider"
import "./index.css"
import ColorModeProvider from "./context/ColourModeContext"
import Topbar from "./components/Topbar"
import Homepage from "./pages/Homepage"
import TemperatureChangePage from "./pages/TemperatureChangePage"
import CountryYearlyDataPage from "./pages/CountryYearlyDataPage"
import EnergyDataPage from "./pages/EnergyDataPage"
import EmissionsDataPage from "./pages/EmissionsDataPage"
import ClimateContributionPage from "./pages/ClimateContributionPage"

/**
 * This component is the main app.
 * 
 * @returns - The component
 */
function App() {
    return (
        <BackendProvider>
            <ColorModeProvider>
                <BrowserRouter>
                    <Topbar />
                    <Routes>
                        <Route path="/" element={<Homepage />} />
                        <Route path="/temperature-changes" element={<TemperatureChangePage />} />
                        <Route path="/country-yearly-data" element={<CountryYearlyDataPage />} />
                        <Route path="/energy-data" element={<EnergyDataPage />} />
                        <Route path="/emissions-data" element={<EmissionsDataPage />} />
                        <Route path="/climate-contribution" element={<ClimateContributionPage />} />
                    </Routes>
                </BrowserRouter>
            </ColorModeProvider>
        </BackendProvider>
    )
}

export default App
