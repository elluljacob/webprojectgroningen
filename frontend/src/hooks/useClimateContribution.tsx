import { useState } from "react";
import ClimateContribution from "../models/ClimateContributionData.model";
import { getClimateContribution } from "../api/ClimateContribution.api";
import { useBackend } from "../context/BackendProvider";
import Filter from "../models/FilterParams.model";
import OrderParams from "../models/Order.model";

export function useClimateContribution() {
    const [data, setData] = useState<ClimateContribution[]>();
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState<Error | null>(null);
    const { selectedBackend } = useBackend();

    const fetchData = (filterParams: Filter, orderParams: OrderParams) => {
        setLoading(true);
        getClimateContribution(selectedBackend, filterParams, orderParams)
            .then(setData)
            .catch(setError)
            .finally(() => setLoading(false));
    };

    return { data, loading, error, fetchData };
}
