import { useState } from "react";
import TemperatureChangeData from "../models/TemperatureChangeData.model";
import { getTemperatureChangeData } from "../api/TemperatureChangeData.api";
import { useBackend } from "../context/BackendProvider";
import Filter from "../models/FilterParams.model";
import OrderParams from "../models/Order.model";

export function useTemperatureChangeData() {
    const [data, setData] = useState<TemperatureChangeData[]>([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState<Error | null>(null);

    const { selectedBackend } = useBackend();

    const fetchData = async (continentName: string, year?: number, limit?: number, filterParams?: Filter, orderParams?: OrderParams) => {
        setLoading(true);
        try {
            const data = await getTemperatureChangeData(selectedBackend, continentName, year, limit, filterParams ?? {}, orderParams ?? {});
            setData(data);
            setError(null);
        } catch (error) {
            setError(error as Error);
        } finally {
            setLoading(false);
        }
    };
    console.log("Data fetched:", data);
    return { data, loading, error, fetchData };
}