import { useState } from "react";
import CountryYearlyData from "../models/CountryYearlyData.model";
import {
    getPopulation,
    getGDP,
    addPopulation,
    updatePopulation,
    deletePopulation,
    addGDP,
    updateGDP,
    deleteGDP
} from "../api/CountryYearlyData.api";
import { useBackend } from "../context/BackendProvider";

export default function useCountryYearlyData(countryIsoCode: string, year: number) {
    const [data, setData] = useState<CountryYearlyData>();
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState<Error | null>(null);

    const { selectedBackend } = useBackend();

    const fetchData = async () => {
        setLoading(true);
        try {
            const populationResult = await getPopulation(selectedBackend, countryIsoCode, year);
            const gdpResult = await getGDP(selectedBackend, countryIsoCode, year);
            setData({
                country: countryIsoCode,
                ISOCode: countryIsoCode,
                year: year,
                GDP: gdpResult,
                population: populationResult
            });
            setError(null);
        } catch (e) {
            setError(e as Error);
        } finally {
            setLoading(false);
        }
    };

    const addData = async (population: number, gdp: number) => {
        setLoading(true);
        try {
            const newPopulation = await addPopulation(selectedBackend, countryIsoCode, year, population);
            const newGdp = await addGDP(selectedBackend, countryIsoCode, year, gdp);
            setData({
                country: countryIsoCode,
                ISOCode: countryIsoCode,
                year: year,
                GDP: newGdp,
                population: newPopulation
            });
            setError(null);
        } catch (e) {
            setError(e as Error);
        } finally {
            setLoading(false);
        }
    };// using both addPop/GDP simplifies it a bit

    const updateData = async (population: number, gdp: number) => {
        setLoading(true);
        try {
            const newPopulation = await updatePopulation(selectedBackend, countryIsoCode, year, population);
            const newGDP = await updateGDP(selectedBackend, countryIsoCode, year, gdp);
            setData({
                country: countryIsoCode,
                ISOCode: countryIsoCode,
                year: year,
                GDP: newGDP,
                population: newPopulation
            });
            setError(null);
        } catch (e) {
            setError(e as Error);
        } finally {
            setLoading(false);
        }
    };// using both updatePop/GDP simplifies it a bit plus you generally want to do both In my opinion

    const removePopulationData = async () => {
        setLoading(true);
        try {
            await deletePopulation(selectedBackend, countryIsoCode, year);
            setData(undefined);
            setError(null);
        } catch (e) {
            setError(e as Error);
        } finally {
            setLoading(false);
        }
    };

    const removeGDPData = async () => {
        setLoading(true);
        try {
            await deleteGDP(selectedBackend, countryIsoCode, year);
            setData(undefined);
            setError(null);
        } catch (e) {
            setError(e as Error);
        } finally {
            setLoading(false);
        }
    };


    return {
        data,
        loading,
        error,
        fetchData,
        addData,
        updateData,
        removePopulationData,
        removeGDPData
    };
}
