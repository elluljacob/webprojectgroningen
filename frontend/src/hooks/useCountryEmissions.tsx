import { useState } from "react";
import EmissionsData from "../models/EmissionsData.model";
import { getCountryEmissions } from "../api/CountryEmissions.api";
import { useBackend } from "../context/BackendProvider";
import Filter from "../models/FilterParams.model";
import OrderParams from "../models/Order.model";

export function useCountryEmissions() {
    const [data, setData] = useState<EmissionsData[]>();
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState<Error | null>(null);

    const { selectedBackend } = useBackend();

    const fetchData = async (countryIsoCode: string, filterParams: Filter, orderParams: OrderParams, limit: number, offset: number) => {
        setLoading(true);
        try {
            const data = await getCountryEmissions(selectedBackend, countryIsoCode, filterParams, orderParams, limit, offset);
            setData(data);
            setError(null);
        } catch (error) {
            setError(error as Error);
        } finally {
            setLoading(false);
        }
    };

    return { data, loading, error, fetchData };
}

