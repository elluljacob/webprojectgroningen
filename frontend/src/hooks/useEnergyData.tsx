import { useState } from "react";
import EnergyData from "../models/EnergyData.model";
import { getEnergyData } from "../api/EnergyData.api";
import Filter from "../models/FilterParams.model";
import OrderParams, { OrderDir } from "../models/Order.model";
import { useBackend } from "../context/BackendProvider";

export function useEnergyData() {
    const [data, setData] = useState<EnergyData | null>(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState<Error | null>(null);

    const { selectedBackend } = useBackend();

    const fetchData = async (year: number, batchSize: number, offset: number, orderDir: OrderDir) => {
        setLoading(true);
        const filterParams: Filter = { limit: batchSize, offset: offset };
        const orderParams: OrderParams = { direction: orderDir };

        try {
            const result = await getEnergyData(selectedBackend, year, filterParams, orderParams);
            setData(result);
            setError(null);
        } catch (error) {
            setError(error as Error);
        } finally {
            setLoading(false);
        }
    };

    return { data, loading, error, fetchData };
}