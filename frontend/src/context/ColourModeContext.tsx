import { CssBaseline, ThemeProvider, createTheme } from "@mui/material"; //This was copy pasted from the tutorials
import { createContext, useMemo, useState } from "react";

/**
 * This is the context type for the ColorModeContext.
 */
export const ColorModeContext = createContext({
    toggleColorMode: () => { },
    currentTheme: 'light'
});

/**
 * This component provides the ColorModeContext.
 * 
 * @param props - The props for the component.
 * @returns - The component
 */
export default function ColorModeProvider(props: { children: React.ReactNode }) {
    const [mode, setMode] = useState<'light' | 'dark'>(
        localStorage.getItem('colorMode') === 'dark' ? 'dark' : 'light'
    );
    const colorMode = useMemo(
        () => ({
            toggleColorMode: () => {
                setMode((prevMode) => {
                    const updatedMode = prevMode === 'light' ? 'dark' : 'light';
                    localStorage.setItem('colorMode', updatedMode);
                    return updatedMode;
                });
            },
        }),
        [],
    );

    const theme = useMemo(
        () =>
            createTheme({
                palette: {
                    mode,
                },
            }),
        [mode],
    );

    return <ColorModeContext.Provider value={{ ...colorMode, currentTheme: mode }}>
        <ThemeProvider theme={theme}>
            <CssBaseline />
            {props.children}
        </ThemeProvider>
    </ColorModeContext.Provider>;
}
