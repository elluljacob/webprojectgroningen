import React from 'react';
import { createContext, ReactNode } from 'react';
// I know this is really bad design but this was made last minute to fix an issue
// and I didn't have time to improve it.

// define the context type
type BackendProviderContextType = {
    selectedBackend: string;
};

// create the context with a default value
const BackendProviderContext = createContext<BackendProviderContextType>({
    selectedBackend: 'http://localhost:3001',
});

type BackendProviderProps = {
    children: ReactNode;
};

export function BackendProvider({ children }: BackendProviderProps) {
    // Use env vars or default to 'http://localhost:3001 which is the one in .env.example'
    const backendUrl = process.env.BACKENDS || 'http://localhost:3001';

    return (
        <BackendProviderContext.Provider value={{ selectedBackend: backendUrl }}>
            {children}
        </BackendProviderContext.Provider>
    );
}

export function useBackend() {
    return React.useContext(BackendProviderContext);
}
