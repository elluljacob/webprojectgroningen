import React from 'react';
import { useNavigate } from 'react-router-dom';
import { Box, Button, Typography } from '@mui/material';

// Idea here is create a homepage with buttons that go to each page
const Homepage: React.FC = () => {
    const navigate = useNavigate();

    return (
        <Box p={3}>
            <Typography variant="h3" gutterBottom>
                Group-51-Project Homepage
            </Typography>
            <Typography variant="h6" gutterBottom>
                Web Engineering 2023-24
            </Typography>
            <p>
                This is a Homepage, with buttons below containing the link to each endpoint
            </p>

            <Box mt={4} display="flex" flexDirection="column" alignItems="start">
                <Button variant="contained" onClick={() => navigate("/temperature-changes")} sx={{ mb: 2 }}>
                    Temperature Changes
                </Button>
                <Button variant="contained" onClick={() => navigate("/country-yearly-data")} sx={{ mb: 2 }}>
                    Country Yearly Data
                </Button>
                <Button variant="contained" onClick={() => navigate("/emissions-data")} sx={{ mb: 2 }}>
                    Emissions Data
                </Button>
                <Button variant="contained" onClick={() => navigate("/energy-data")} sx={{ mb: 2 }}>
                    Energy Data
                </Button>
                <Button variant="contained" onClick={() => navigate("/climate-contribution")} sx={{ mb: 2 }}>
                    Climate Contribution Data
                </Button>
            </Box>
            <p>
                This web app developed builds upon the “Data on CO2 and Greenhouse Gas Emissions by Our World in Data” dataset.<br></br>
                Providing an interface to interact with a portion of this dataset constrained to the last 50 years.
            </p>
        </Box>
    );
};

export default Homepage;
