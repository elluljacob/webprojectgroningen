import React, { useState } from 'react';
import TemperatureChangeViewer from '../components/TemperatureChangeViewer';
import { useTemperatureChangeData } from '../hooks/useTempChangeData';

const TemperatureChangePage: React.FC = () => {
  const [continent, setContinent] = useState('');
  const [year, setYear] = useState<number | undefined>();
  const [limit, setLimit] = useState<number | undefined>(1);
  const { data, loading, error, fetchData } = useTemperatureChangeData();
  const handleFetchData = () => {
    fetchData(continent, year, limit, {}, {});
  };
  return (
    <div>
      <h1>Temperature Change Data</h1>
      <input
        type="text"
        value={continent}
        onChange={(e) => setContinent(e.target.value)}
        placeholder="Continent Name"
      />
      <input
        type="number"
        value={year}
        onChange={(e) => setYear(e.target.value ? parseInt(e.target.value) : undefined)}
        placeholder="Year"
      />
      <input
        type="number"
        value={limit}
        onChange={(e) => setLimit(e.target.value ? parseInt(e.target.value) : undefined)}
        placeholder="Limit"
      />
      <button onClick={handleFetchData}>Fetch Data</button>
      <TemperatureChangeViewer continentName={continent} year={year} data={data} loading={loading} error={error} />
    </div>
  );
};

export default TemperatureChangePage;