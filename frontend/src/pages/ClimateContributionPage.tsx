import React, { useState } from 'react';
import { ClimateContributionColumns, OrderDir } from '../models/Order.model';
import ClimateContributionViewer from '../components/ClimateContributionViewer';
import { useClimateContribution } from '../hooks/useClimateContribution';

const ClimateContributionPage: React.FC = () => {
    const [filterParams, setFilterParams] = useState({ year: undefined, limit: undefined, offset: undefined });
    const [orderParams, setOrderParams] = useState({ direction: OrderDir.Asc });
    const { data, loading, error, fetchData } = useClimateContribution();

    const handleFilterChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setFilterParams({ ...filterParams, [e.target.name]: parseInt(e.target.value) });
    };

    const handleOrderChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
        setOrderParams({ ...orderParams, [e.target.name]: e.target.value as OrderDir });
    };

    return (
        <div>
            <h1>Climate Contribution Data</h1>
            <input name="year" type="number" placeholder="Year" onChange={handleFilterChange} />
            <input name="limit" type="number" placeholder="Limit" onChange={handleFilterChange} />
            <input name="offset" type="number" placeholder="Offset" onChange={handleFilterChange} />
            <select name="by" onChange={handleOrderChange}>
                <option value={ClimateContributionColumns.Year}>Year</option>
                <option value={ClimateContributionColumns.CO2}>CO2</option>
                <option value={ClimateContributionColumns.Methane}>Methane</option>
                <option value={ClimateContributionColumns.NitrousOxide}>Nitrous Oxide</option>
                <option value={ClimateContributionColumns.TotalGHG}>Total GHG</option>
            </select>
            <select name="direction" onChange={handleOrderChange}>
                <option value={OrderDir.Asc}>Ascending</option>
                <option value={OrderDir.Desc}>Descending</option>
            </select>
            <ClimateContributionViewer
                data={data}
                loading={loading}
                error={error}
                onFetchData={() => fetchData(filterParams, orderParams)}
            />
        </div>
    );
};

export default ClimateContributionPage;