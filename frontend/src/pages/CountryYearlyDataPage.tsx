import React, { useState } from 'react';
import CountryDataForm from '../components/CountryDataForm';
import useCountryYearlyData from '../hooks/useCountryYearlyData';

const CountryYearlyDataPage: React.FC = () => {
    const [countryIsoCode, setCountryIsoCode] = useState('');
    const [year, setYear] = useState<number | undefined>();

    const { data, error, loading, fetchData } = useCountryYearlyData(countryIsoCode, year || 0);

    const handleFetchData = async () => {
        fetchData();
    };

    const handleSubmission = (submissionData: any) => {
        console.log('Form Submitted', submissionData);
    };

    const handleIsoCodeChange = (isoCode: string) => {
        setCountryIsoCode(isoCode);
    };

    const handleYearChange = (year: number | undefined) => {
        setYear(year);
    };

    return (
        <div>
            <h1>Country Yearly Data</h1>
            <p>Note: The "Fetch Data" button, fetches both GDP and Population at once.<br></br>
                Also the Submit button edits a row if the country's data already exists. Otherwise
                it just submits a new row.<br></br>
                Delete Population/GDP data do exactly what they describe.
            </p>
            <input
                type="text"
                value={countryIsoCode}
                onChange={(e) => handleIsoCodeChange(e.target.value)}
                placeholder="Country Name or ISO Code"
            />
            <input
                type="number"
                value={year}
                onChange={(e) => handleYearChange(e.target.value ? parseInt(e.target.value) : undefined)}
                placeholder="Year"
            />
            {loading && <p>Loading...</p>}
            {error && <p>Error: {error.message}</p>}
            {data && !loading && !error && (
                <div>
                    <p>Population: {data.population}</p>
                    <p>GDP: {data.GDP}</p>
                </div>
            )}
            <button onClick={handleFetchData}>Fetch Data</button>
            <CountryDataForm onSubmission={handleSubmission} />
        </div>
    );
};

export default CountryYearlyDataPage;
