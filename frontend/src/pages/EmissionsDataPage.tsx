import React, { useState } from 'react';
import EmissionsDataViewer from '../components/EmissionsDataViewer';
import { useCountryEmissions } from '../hooks/useCountryEmissions';

const EmissionsDataPage: React.FC = () => {
    const [countryIsoCode, setCountryIsoCode] = useState('');
    const [year, setYear] = useState<number>(new Date().getFullYear());
    const [limit, setLimit] = useState<number>(1);
    const [offset, setOffset] = useState<number>(0);
    const { data, loading, error, fetchData } = useCountryEmissions();

    const handleIsoCodeChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setCountryIsoCode(e.target.value);
    };

    const handleYearChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setYear(parseInt(e.target.value));
    };

    const handleLimitChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setLimit(parseInt(e.target.value) || 1);
    };

    const handleOffsetChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setOffset(parseInt(e.target.value) || 0);
    };

    const handleFetchData = () => {
        fetchData(countryIsoCode, { year }, {}, limit, offset);
    };

    return (
        <div>
            <h1>Emissions Data</h1>
            <p>Input Boxes below are "Country or ISOCode", "year", "limit", and lastly "offset" in that order</p>
            <input
                type="text"
                value={countryIsoCode}
                placeholder="Country ISO Code"
                onChange={handleIsoCodeChange}
            />
            <input
                type="number"
                value={year}
                placeholder="Year"
                onChange={handleYearChange}
            />
            <input
                type="number"
                value={limit}
                placeholder="Limit"
                onChange={handleLimitChange}
            />
            <input
                type="number"
                value={offset}
                placeholder="Offset"
                onChange={handleOffsetChange}
            />
            <button onClick={handleFetchData}>Fetch Data</button>
            <EmissionsDataViewer
                countryIsoCode={countryIsoCode}
                year={year}
                data={data}
                loading={loading}
                error={error}
            />
        </div>
    );
};

export default EmissionsDataPage;
