import React from 'react';
import EnergyDataViewer from '../components/EnergyDataViewer';

const EnergyDataPage: React.FC = () => {
    return (
        <div>
            <h1>Energy Data</h1>
            <EnergyDataViewer />
        </div>
    );
};

export default EnergyDataPage;
