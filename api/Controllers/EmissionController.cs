using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RUG.WebEng.Api.Models;
using RUG.WebEng.Api.Repositories;
using RUG.WebEng.Api.CustomExceptions;
using System.Text;


namespace RUG.WebEng.Api.Controllers;

[ApiController]
[Route("countries")]
public class EmissionsController : AbstractController
{
    private readonly EmissionsRepository _emissionsRepository;
    private readonly CountryYearlyDataRepository _countryYearlyDataRepository;

    public EmissionsController(EmissionsRepository emissionsRepository, CountryYearlyDataRepository countryYearlyDataRepository)
    {
        _emissionsRepository = emissionsRepository;
        _countryYearlyDataRepository = countryYearlyDataRepository;
    }

    [HttpGet("emissions")]
    public async Task<IActionResult> GetEmissions(
    string isoOrCountry,
    [FromQuery] RequestModels.Order order,
    [FromQuery] Models.Paging<Emissions> paging,
    [FromQuery] RequestModels.Filter filter
)
    {
        var country = await _countryYearlyDataRepository.FindAsync((isoOrCountry, filter.Year ?? 1973));
        if (country == null)
            throw new InvalidInputException(isoOrCountry);

        var data = Models.IApiQuery<Emissions>.ApplyAll(
            _emissionsRepository.Collection,
            filter,
            order,
            paging);

        if (data == null)
            return NotFound("No data found for this continent.");

        var acceptHeader = Request.Headers["Accept"].FirstOrDefault();
        if (acceptHeader != null && acceptHeader.Contains("text/csv"))
        {
            var csvResponse = ConvertDataToCsv(data);
            return File(Encoding.UTF8.GetBytes(csvResponse), "text/csv", "data.csv");
        }
        else
        {

            return Ok(data.AsAsyncEnumerable().Select(ApiModels.EmissionsData.FromDatabase));
        }
    }
    private string ConvertDataToCsv(IEnumerable<Emissions> data)// Not great but this is the easiest solution I found
    { //Since the CsvOutputFormatter breaks with this controller I made this custom one (primitive and might be a better way of doing it but this is what I know works) 
        var csv = new StringBuilder();

        // Add CSV header
        csv.AppendLine("country,isoCode,year,cO2,methane,nitrousOxide,totalGHG");

        foreach (var item in data)
        {
            // Convert each object to a CSV row
            csv.AppendLine($"{item.Country},{item.ISOCode},{item.Year},{item.CO2},{item.Methane},{item.NitrousOxide},{item.TotalGHG}");
        }

        return csv.ToString();
    }

}
