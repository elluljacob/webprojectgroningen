using Microsoft.AspNetCore.Mvc;
using RUG.WebEng.Api.Models;
using RUG.WebEng.Api.Repositories;
using RUG.WebEng.Api.CustomExceptions;

namespace RUG.WebEng.Api.Controllers;

[ApiController]
[Route("countries")]
public class PopulationController : AbstractController 
{
    private readonly CountryYearlyDataRepository _countryYearlyDataRepository;

    public PopulationController(CountryYearlyDataRepository countryYearlyDataRepository)
    {
        _countryYearlyDataRepository = countryYearlyDataRepository;
    }

    [HttpGet("population")]
    public async Task<ActionResult<double>> GetPopulation(string isoOrCountry, int year)
    {
        var data = await _countryYearlyDataRepository.FindAsync((isoOrCountry, year));
        if (data == null)
            throw new InvalidInputException(isoOrCountry);
        
        return Ok(data.Population);
    }

    [HttpPost("population")]
    public async Task<ActionResult<double>> AddPopulation(string isoOrCountry, int year, double population)
    {
        var data = await _countryYearlyDataRepository.FindAsync((isoOrCountry, year));
        
        if (data != null && data.Population != null)
            throw new DataAlreadyExistsException(year, isoOrCountry);

        if (data == null)
        {
            data = new CountryYearlyData
            {
                ISOCode = "404",
                Country = "unknown",
                Year = year,
                Population = population
            };

            if (isoOrCountry.Length == 3)
                data.ISOCode = isoOrCountry;
            else
                data.Country = isoOrCountry;
        }
        else    
            data.Population = population;

        await _countryYearlyDataRepository.CreateAsync(data);
        return Ok(data.Population);
    }

    [HttpPatch("population")]
    public async Task<ActionResult<double>> UpdatePopulation(string isoOrCountry, int year, double population)
    {
        var data = await _countryYearlyDataRepository.FindAsync((isoOrCountry, year));
        if (data == null)
            throw new DataNotFoundException(year, isoOrCountry);
        
        data.Population = population;
        await _countryYearlyDataRepository.UpdateAsync(data);
        return Ok(data.Population);
    }

    [HttpDelete("population")]
    public async Task<IActionResult> DeletePopulation(string isoOrCountry, int year)
    {
        var data = await _countryYearlyDataRepository.FindAsync((isoOrCountry, year));
        if (data == null || data.Population == null)
            throw new DataNotFoundException(year, isoOrCountry);

        data.Population = null;
        await _countryYearlyDataRepository.UpdateAsync(data);
        return NoContent();
    }
}
