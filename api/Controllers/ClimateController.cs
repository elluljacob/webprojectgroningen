using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RUG.WebEng.Api.Models;
using RUG.WebEng.Api.Repositories;
using RUG.WebEng.Api.ApiModels;

namespace RUG.WebEng.Api.Controllers;
//REQ1/5
[ApiController]
[Route("climate-contribution")]
public class ClimateChangeContributionController : AbstractController
{
    private readonly EmissionsRepository _emissionsRepository;

    public ClimateChangeContributionController(EmissionsRepository emissionsRepository)
    {
        _emissionsRepository = emissionsRepository;
    }

    [HttpGet("countries")]
    public async Task<IActionResult> GetClimateChangeContribution(
        [FromQuery] RequestModels.Order order,
        [FromQuery] Models.Paging<Emissions> paging,
        [FromQuery] RequestModels.Filter filter)
    {
        var query =  Models.IApiQuery<Emissions>.ApplyAll(
            _emissionsRepository.Collection,
            filter,
            order,
            paging);
       
        var topCountries = await query.Select(e => TemperatureChangeData.FromDatabase(e)).ToListAsync();

        return Ok(topCountries);
    }
}

