using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RUG.WebEng.Api.Models;
using RUG.WebEng.Api.Repositories;
using RUG.WebEng.Api.ApiModels;
using RUG.WebEng.Api.CustomExceptions;

namespace RUG.WebEng.Api.Controllers;
//REQ1/4
[ApiController]
[Route("energy")]
public class EnergyDataController : AbstractController
{
    private readonly CountryYearlyDataRepository _countryYearlyDataRepository;

    public EnergyDataController(CountryYearlyDataRepository countryYearlyDataRepository)
    {
        _countryYearlyDataRepository = countryYearlyDataRepository;
    }

    [HttpGet("{year}")]
    public async Task<IActionResult> GetEnergyData(int year, int limit, int offset)
    {
        int[] validBatchSizes = {10, 20, 50, 100};
        if (!validBatchSizes.Contains(limit))
        {
            return BadRequest("Invalid batch size. Valid sizes are 10, 20, 50, 100.");
        }
        var energyDataQuery = _countryYearlyDataRepository.Collection
                                .Where(d => d.Year == year)
                                .OrderBy(d => d.Population) // Sort by Population
                                .Skip(offset)
                                .Take(limit)
                                .Select(d => new EnergyData
                                {
                                    Country = d.Country,
                                    Year = d.Year,
                                    EnergyPerCapita = d.EnergyPerCapita ?? 0,
                                    EnergyPerGDP = d.EnergyPerGDP ?? 0
                                });

        var energyData = await energyDataQuery.ToListAsync();

        if (!energyData.Any())
            throw new EnergyDataNotFoundException(year);

        return Ok(energyData);
    }
}
