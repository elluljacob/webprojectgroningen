using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RUG.WebEng.Api.Models;
using RUG.WebEng.Api.Repositories;
using RUG.WebEng.Api.CustomExceptions;

namespace RUG.WebEng.Api.Controllers;

[ApiController]
[Route("countries")]
public class GDPController : AbstractController 
{
    private readonly CountryYearlyDataRepository _countryYearlyDataRepository;
    private readonly EmissionsRepository _emissionsRepository;

    public GDPController(CountryYearlyDataRepository countryYearlyDataRepository, EmissionsRepository emissionsRepository)
    {
        _countryYearlyDataRepository = countryYearlyDataRepository;
        _emissionsRepository = emissionsRepository;
    }

    [HttpGet("gdp")]
    public async Task<ActionResult<double>> GetGDP(string isoOrCountry, int year)
    {
        var data = await _countryYearlyDataRepository.FindAsync((isoOrCountry, year));
        if (data == null || data.GDP == null)
            throw new InvalidInputException(isoOrCountry);

        return Ok(data.GDP);
    }

    [HttpPost("gdp")]
    public async Task<ActionResult<double>> AddGDP(string isoOrCountry, int year, double gdp)
    {
        var data = await _countryYearlyDataRepository.FindAsync((isoOrCountry, year));

        if (data != null && data.GDP != null)
            throw new DataAlreadyExistsException(year, isoOrCountry);
        
        if (data == null)
        {
            data = new CountryYearlyData
            {
                ISOCode = "404",
                Country = "unknown",
                Year = year,
                GDP = gdp
            };

            if (isoOrCountry.Length == 3)
                data.ISOCode = isoOrCountry;
            else
                data.Country = isoOrCountry;
        }
        else   
            data.GDP = gdp;

        await _countryYearlyDataRepository.CreateAsync(data);
        return Ok(data.GDP);
    }


    [HttpPatch("gdp")]
    public async Task<ActionResult<double>> UpdateGDP(string isoOrCountry, int year, double gdp)
    {
        var data = await _countryYearlyDataRepository.FindAsync((isoOrCountry, year));
        if (data == null)
            throw new DataNotFoundException(year, isoOrCountry);

        data.GDP = gdp;
        await _countryYearlyDataRepository.UpdateAsync(data);
        return Ok(data.GDP);
    }

    [HttpDelete("gdp")]
    public async Task<IActionResult> DeleteGDP(string isoOrCountry, int year)
    {
        var data = await _countryYearlyDataRepository.FindAsync((isoOrCountry, year));
        if (data == null || data.GDP == null)
            throw new DataNotFoundException(year, isoOrCountry);

        data.GDP = null;
        await _countryYearlyDataRepository.UpdateAsync(data);
        return NoContent();
    }
}