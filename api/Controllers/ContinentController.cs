using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RUG.WebEng.Api.Models;
using RUG.WebEng.Api.Repositories;
using RUG.WebEng.Api.CustomExceptions;

namespace RUG.WebEng.Api.Controllers;

[ApiController]
[Route("continents")]
public class ContinentController : AbstractController
{
    private readonly EmissionsRepository _emissionsRepository;

    private enum Continents { Africa, Asia, Europe, NorthAmerica, Oceania, SouthAmerica }
    public ContinentController(EmissionsRepository emissionsRepository)
    {
        _emissionsRepository = emissionsRepository;
    }
    //REQ1/3
    [HttpGet("temperature-changes")]
    public async Task<ActionResult<IAsyncEnumerable<ApiModels.TemperatureChangeData>>> GetTemperatureChanges(
        [FromQuery] string continent,
        [FromQuery] int? year,
        [FromQuery] RequestModels.Order order,
        [FromQuery] Models.Paging<Emissions> paging,
        [FromQuery] RequestModels.Filter filter
        )
    {
        if (!ModelState.IsValid)
            throw new BadModelStateException();

        if (!Enum.TryParse<Continents>(continent, true, out var continentEnum))
            throw new InvalidContinentException(continent);

        var emissionsQuery = _emissionsRepository.Collection
            .Where(e => e.Country == continent);

        if (year.HasValue)
        {
            emissionsQuery = emissionsQuery.Where(e => e.Year >= year.Value);
        }

        emissionsQuery = Models.IApiQuery<Emissions>.ApplyAll(emissionsQuery, filter, order, paging);

        var temperatureChangeData = await emissionsQuery
            .Select(e => ApiModels.TemperatureChangeData.FromDatabase(e))
            .ToListAsync();

        return Ok(temperatureChangeData);
    }
}