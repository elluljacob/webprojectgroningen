using Microsoft.AspNetCore.Mvc.Formatters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace RUG.WebEng.Api.CsvOutputFormatter
{
    public class CsvOutputFormatter : TextOutputFormatter
    {
        public CsvOutputFormatter()
        {
            SupportedMediaTypes.Add("text/csv");
            SupportedEncodings.Add(Encoding.UTF8);
        }

        public override async Task WriteResponseBodyAsync(OutputFormatterWriteContext context, Encoding selectedEncoding)
        {
            var response = context.HttpContext.Response;

            if (context.Object is int || context.Object is double || context.Object is float)
            {
                // case where response is a single number (GDP and population)
                var valueString = context.Object.ToString();
                await response.WriteAsync(valueString, selectedEncoding);
            }
            else
            {
                // case where the response is JSON (everything else)
                var jsonString = JsonConvert.SerializeObject(context.Object);

                try
                {
                    var jsonArray = JArray.Parse(jsonString);
                    var csvData = ConvertJsonToCsv(jsonArray);

                    // Write the CSV data to the response
                    await response.WriteAsync(csvData, selectedEncoding);
                }
                catch (Exception ex)
                {
                    await response.WriteAsync("Error processing request: " + ex.Message);
                }
            }
        }


        private string ConvertJsonToCsv(JArray jsonArray)
        {
            var csv = new StringBuilder();

            if (jsonArray.Count > 0)
            {
                var headerItems = jsonArray[0].Children<JProperty>().Select(x => x.Name);
                csv.AppendLine(string.Join(",", headerItems));

                foreach (var item in jsonArray)
                {
                    var row = item.Children<JProperty>().Select(x => x.Value.ToString());
                    csv.AppendLine(string.Join(",", row));
                }
            }

            return csv.ToString();
        }
    }
}