using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RUG.WebEng.Api.Models;
using RUG.WebEng.Api.Repositories;

namespace RUG.WebEng.Api.Controllers
{
    public class RequestModels
    {
        public class Order : AbstractController.Models.IApiQuery<Emissions>
        {
            public enum Column { Year, CO2, Methane, NitrousOxide, TotalGHG }
            public enum Dir { Asc, Desc }

            [BindProperty(Name = "order-by")]
            public Column? By { get; set; }
            
            [BindProperty(Name = "order-dir")]
            public Dir? Direction { get; set; }

            public IQueryable<Emissions> Apply(IQueryable<Emissions> emissions) =>
                (By, Direction) switch
                {
                    (Column.Year, Dir.Asc) => emissions.OrderBy(m => m.Year),
                    (Column.Year, Dir.Desc) => emissions.OrderByDescending(m => m.Year),
                    (Column.CO2, Dir.Asc) => emissions.OrderBy(m => m.CO2),
                    (Column.CO2, Dir.Desc) => emissions.OrderByDescending(m => m.CO2),
                    (Column.Methane, Dir.Asc) => emissions.OrderBy(m => m.Methane),
                    (Column.Methane, Dir.Desc) => emissions.OrderByDescending(m => m.Methane),
                    (Column.NitrousOxide, Dir.Asc) => emissions.OrderBy(m => m.NitrousOxide),
                    (Column.NitrousOxide, Dir.Desc) => emissions.OrderByDescending(m => m.NitrousOxide),
                    (Column.TotalGHG, Dir.Asc) => emissions.OrderBy(m => m.TotalGHG),
                    (Column.TotalGHG, Dir.Desc) => emissions.OrderByDescending(m => m.TotalGHG),
                    _ => emissions,
                };
        }
        public class Filter : AbstractController.Models.IApiQuery<Emissions>
        {
            public int? Year { get; set; }

            public IQueryable<Emissions> Apply(IQueryable<Emissions> emissions)
            {
                if (Year != null) emissions = emissions.Where(m => m.Year >= Year);
                return emissions;
            }
        }
    }
}
