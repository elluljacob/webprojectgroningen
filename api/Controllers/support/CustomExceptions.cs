using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RUG.WebEng.Api.Models;
using RUG.WebEng.Api.Repositories;

namespace RUG.WebEng.Api.CustomExceptions
{
    public class InvalidInputException : Exception
    {
        public string WrongValue { get; private set; }

        public InvalidInputException(string wrongValue)
            : base($"Invalid ISO code or Country: {wrongValue}")
        {
            WrongValue = wrongValue;
        }
    }

    public class DataAlreadyExistsException : Exception
    {
        public int WrongYear { get; private set; }
        public string WrongCountry { get; private set; }

        public DataAlreadyExistsException(int year, string country)
            : base($"Data already exists for year: {year}, country: {country}")
        {
            WrongYear = year;
            WrongCountry = country;
        }
    }

    public class DataNotFoundException : Exception
    {
        public int WrongYear { get; private set; }
        public string WrongCountry { get; private set; }

        public DataNotFoundException(int year, string country)
            : base($"No data found for year: {year}, country: {country}")
        {
            WrongYear = year;
            WrongCountry = country;
        }
    }

    public class BadModelStateException : Exception
    {
    }

    public class InvalidContinentException : Exception
    {
        public string WrongValue { get; private set; }

        public InvalidContinentException(string wrongValue)
            : base($"Invalid continent name: {wrongValue}")
        {
            WrongValue = wrongValue;
        }
    }
    public class EnergyDataNotFoundException : Exception
    {
        public int year { get; private set; }

        public EnergyDataNotFoundException(int WrongValue)
            : base($"No energy data found for year: {WrongValue}")
        {
            year = WrongValue;
        }
    }
}
