using System.Text.Json.Serialization;
using Microsoft.EntityFrameworkCore;
using RUG.WebEng.Api.Models;
using RUG.WebEng.Api.Repositories;
using RUG.WebEng.Api.CsvOutputFormatter;

DotNetEnv.Env.TraversePath().NoClobber().LoadMulti([".env", "dotnet.env"]);

var builder = WebApplication.CreateBuilder(args);

// Add database connection
var dbConnectionString = builder.Configuration.GetConnectionString("DefaultConnection");
Console.WriteLine($"Using database connection string: {dbConnectionString}");
Thread.Sleep(500);// just ensure db is set up before we connect to it
builder.Services.AddDbContext<DatabaseContext>(options =>
    options.UseMySql(dbConnectionString, ServerVersion.AutoDetect(dbConnectionString)));

// We say that in developer mode, Entity Framework might give us some hints
// about database errors
builder.Services.AddDatabaseDeveloperPageExceptionFilter();

// Add repository classes we created ourselves - see docs over there for more
// We register them "scoped" so they are recreated for every new request - the
// default for database connections
builder.Services.AddScoped<CountryYearlyDataRepository>();
builder.Services.AddScoped<EmissionsRepository>();

// Say we will be using controller classes
builder.Services.AddControllers().AddJsonOptions(options =>
{
    // Indicate we would like ENUM values to be converted to strings in their
    // JSON representation, as opposed to meaningless integers
    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
})
.AddMvcOptions(options =>
{
    options.OutputFormatters.Add(new CsvOutputFormatter());
});// option to have csv instead of json

// Say we will be using tools to analyse the exposed API surface
builder.Services.AddEndpointsApiExplorer();

// Say we will be exposing automatically-generated Swagger/OpenAPI docs
builder.Services.AddSwaggerGen(config =>
{
    config.EnableAnnotations(); // we will use attributes for further specification
});

// Register a CORS policy that allows traffic from all sources (do NOT use this
// in this form for real production applications!)
builder.Services.AddCors(options =>
{
    options.AddPolicy("default", builder =>
        builder
            .AllowAnyHeader()
            .AllowAnyMethod()
            .AllowAnyOrigin()
    );
});

// ---------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------

// Then we build the app and its IoC container, so we can configure
// the lifecycle, including routing and middleware.
var app = builder.Build();

// We only enable auto-seeding of the database if we enable the preprocessor step
// below. Default is disabled since this takes over 45 minutes - it is quicker to
// use the provided `.sql` files and import those.
#if true
using var scope = app.Services.CreateScope();
using var context = scope.ServiceProvider.GetRequiredService<DatabaseContext>();
await DatabaseSeeder.InitializeAsync(context);
#endif

// Specify which CORS policy to use - registered above
app.UseCors("default");

// If we are in development, we enable Swagger UI
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

// Find all Controller classes/implementations in the current application and
// register their routes and operations.
app.MapControllers();

// Run the app
app.Run();
