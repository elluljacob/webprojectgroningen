using Microsoft.EntityFrameworkCore;
using RUG.WebEng.Api.Models;

namespace RUG.WebEng.Api.Models;

public class DatabaseContext : DbContext
{
    public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
    {
    }

    public DbSet<CountryYearlyData> CountryYearlyData { get; set; }
    public DbSet<Emissions> Emissions { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // Config for composite keys
        modelBuilder.Entity<CountryYearlyData>()
            .HasKey(cyd => new { cyd.Country, cyd.Year });

        modelBuilder.Entity<Emissions>()
            .HasKey(em => new { em.Country, em.Year });
    }
}
