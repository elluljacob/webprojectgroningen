using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RUG.WebEng.Api.Models;

public class CountryYearlyData
{
    [Key, Column(Order = 0)]
    [Required]
    public required string Country { get; set; }

    [Key, Column(Order = 1)]
    [Required]
    public int Year { get; set; }
    public required string ISOCode { get; set; }
    public double? GDP { get; set; }
    public double? Population { get; set; }
    public double? EnergyPerCapita { get; set; }
    public double? EnergyPerGDP { get; set; }
}
