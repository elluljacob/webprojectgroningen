using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.EntityFrameworkCore;
using RUG.WebEng.Api.Models;
using System.IO;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace RUG.WebEng.Api.Models;

public class DatabaseSeeder
{
    public class IntConverter : JsonConverter<int>
    {
        public override int Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            if (reader.TokenType == JsonTokenType.String)
            {
                string stringValue = reader.GetString();
                if (int.TryParse(stringValue, out int value))
                {
                    return value;
                }
                throw new JsonException("Unable to convert string to integer.");
            }
            return reader.GetInt32();
        }

        public override void Write(Utf8JsonWriter writer, int value, JsonSerializerOptions options)
        {
            writer.WriteNumberValue(value);
        }
    }

    public class DoubleConverter : JsonConverter<double?>
    {
        public override double? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            if (reader.TokenType == JsonTokenType.String)
            {
                string stringValue = reader.GetString();
                if (double.TryParse(stringValue, out double value))
                {
                    return value;
                }
                return null;
            }
            return reader.GetDouble();
        }

        public override void Write(Utf8JsonWriter writer, double? value, JsonSerializerOptions options)
        {
            writer.WriteNumberValue(value.GetValueOrDefault());
        }
    }

    private class JsonDataModel
    {
        [JsonPropertyName("country")] public required string Country { get; set; }

        [JsonPropertyName("iso_code")] public required string IsoCode { get; set; }

        [JsonPropertyName("year")][JsonConverter(typeof(IntConverter))] public int Year { get; set; }

        [JsonPropertyName("gdp")][JsonConverter(typeof(DoubleConverter))] public double? GDP { get; set; }

        [JsonPropertyName("population")][JsonConverter(typeof(DoubleConverter))] public double? Population { get; set; }

        [JsonPropertyName("energy_per_capita")][JsonConverter(typeof(DoubleConverter))] public double? EnergyPerCapita { get; set; }

        [JsonPropertyName("energy_per_gdp")][JsonConverter(typeof(DoubleConverter))] public double? EnergyPerGDP { get; set; }

        [JsonPropertyName("temperature_change_from_ch4")][JsonConverter(typeof(DoubleConverter))] public double? TemperatureChangeFromCH4 { get; set; }

        [JsonPropertyName("temperature_change_from_n2o")][JsonConverter(typeof(DoubleConverter))] public double? TemperatureChangeFromN2O { get; set; }

        [JsonPropertyName("temperature_change_from_nco2")][JsonConverter(typeof(DoubleConverter))] public double? TemperatureChangeFromCO2 { get; set; }

        [JsonPropertyName("share_of_temperature_change_from_ghg")][JsonConverter(typeof(DoubleConverter))] public double? ShareOfTemperatureChangeFromGHG { get; set; }

        [JsonPropertyName("co2")][JsonConverter(typeof(DoubleConverter))] public double? CO2 { get; set; }

        [JsonPropertyName("methane")][JsonConverter(typeof(DoubleConverter))] public double? Methane { get; set; }

        [JsonPropertyName("nitrous_oxide")][JsonConverter(typeof(DoubleConverter))] public double? NitrousOxide { get; set; }

        [JsonPropertyName("total_ghg")][JsonConverter(typeof(DoubleConverter))] public double? TotalGHG { get; set; }
    }

    public static async Task InitializeAsync(DatabaseContext context)
    {
        await context.Database.EnsureCreatedAsync();
        if (await context.CountryYearlyData.AnyAsync() || await context.Emissions.AnyAsync()) return;

        var dataFile = File.OpenRead("../database/data.json");
        var results = await JsonSerializer.DeserializeAsync<List<JsonDataModel>>(dataFile) ?? new List<JsonDataModel>();

        foreach (var item in results)
        {
            var countryYearlyData = await context.CountryYearlyData
                .FindAsync(item.Country, item.Year);
            if (countryYearlyData == null)
            {
                countryYearlyData = new CountryYearlyData
                {
                    Country = item.Country,
                    Year = item.Year,
                    ISOCode = item.IsoCode,
                    GDP = item.GDP,
                    Population = item.Population,
                    EnergyPerCapita = item.EnergyPerCapita,
                    EnergyPerGDP = item.EnergyPerGDP
                };
                context.CountryYearlyData.Add(countryYearlyData);
            }

            var emissions = await context.Emissions
                .FindAsync(item.Country, item.Year);
            if (emissions == null)
            {
                emissions = new Emissions
                {
                    Country = item.Country,
                    Year = item.Year,
                    ISOCode = item.IsoCode,
                    TemperatureChangeFromCH4 = item.TemperatureChangeFromCH4,
                    TemperatureChangeFromN2O = item.TemperatureChangeFromN2O,
                    TemperatureChangeFromCO2 = item.TemperatureChangeFromCO2,
                    ShareOfTemperatureChangeFromGHG = item.ShareOfTemperatureChangeFromGHG,
                    CO2 = item.CO2,
                    Methane = item.Methane,
                    NitrousOxide = item.NitrousOxide,
                    TotalGHG = item.TotalGHG
                };
                context.Emissions.Add(emissions);
            }
        }

        await context.SaveChangesAsync();
    }
}
