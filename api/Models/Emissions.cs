using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RUG.WebEng.Api.Models;

public class Emissions
{
    [Key, Column(Order = 0)]
    [Required]
    public required string Country { get; set; }

    [Key, Column(Order = 1)]
    [Required]
    public int Year { get; set; }
    public required string ISOCode { get; set; }
    public double? TemperatureChangeFromCH4 { get; set; }
    public double? TemperatureChangeFromN2O { get; set; }
    public double? TemperatureChangeFromCO2 { get; set; }
    public double? ShareOfTemperatureChangeFromGHG { get; set; }
    public double? CO2 { get; set; }
    public double? Methane { get; set; }
    public double? NitrousOxide { get; set; }
    public double? TotalGHG { get; set; }
}
