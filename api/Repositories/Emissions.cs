namespace RUG.WebEng.Api.Repositories;

using Microsoft.EntityFrameworkCore;
using RUG.WebEng.Api.Models;

public class EmissionsRepository : IRepository<Emissions, (string Country, int Year)>
{
    protected DatabaseContext _context;

    public EmissionsRepository(DatabaseContext context)
    {
        _context = context;
    }

    public IQueryable<Emissions> Collection => _context.Emissions;

    public async Task<bool> CreateAsync(Emissions emissions)
    {
        if (await _context.Emissions.AnyAsync(e => (e.Country == emissions.Country || e.ISOCode == emissions.ISOCode) && e.Year == emissions.Year))
            return false;

        _context.Emissions.Add(emissions);
        await _context.SaveChangesAsync();
        return true;
    }


    public async Task DeleteAsync(Emissions emissions)
    {
        _context.Emissions.Remove(emissions);
        await _context.SaveChangesAsync();
    }

    public async Task<bool> UpdateAsync(Emissions emissions)
    {
        _context.Update(emissions);
        await _context.SaveChangesAsync();
        return true;
    }

    public async Task<Emissions?> FindAsync((string Country, int Year) key) => 
        await Collection.FirstOrDefaultAsync(e => (e.Country == key.Country || e.ISOCode == key.Country) && e.Year == key.Year);
}
