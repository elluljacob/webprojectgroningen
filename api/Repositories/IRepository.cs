namespace RUG.WebEng.Api.Repositories;
public interface IRepository<T, TKey> where T : class
{
    public Task<bool> CreateAsync(T item);
    public Task DeleteAsync(T item);
    public Task<bool> UpdateAsync(T item);
    public Task<T?> FindAsync(TKey key);
    public IQueryable<T> Collection { get; }
}
