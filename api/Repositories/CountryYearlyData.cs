namespace RUG.WebEng.Api.Repositories;

using Microsoft.EntityFrameworkCore;
using RUG.WebEng.Api.Models;

public class CountryYearlyDataRepository : IRepository<CountryYearlyData, (string Country, int Year)>
{
    protected DatabaseContext _context;

    public CountryYearlyDataRepository(DatabaseContext context)
    {
        _context = context;
    }

    public IQueryable<CountryYearlyData> Collection => _context.CountryYearlyData;

    public async Task<bool> CreateAsync(CountryYearlyData data)
    {
        if (await _context.CountryYearlyData.AnyAsync(d => (d.Country == data.Country || d.ISOCode == data.ISOCode) && d.Year == data.Year))
            return false;

        _context.CountryYearlyData.Add(data);
        await _context.SaveChangesAsync();
        return true;
    }


    public async Task DeleteAsync(CountryYearlyData data)
    {
        _context.CountryYearlyData.Remove(data);
        await _context.SaveChangesAsync();
    }

    public async Task<bool> UpdateAsync(CountryYearlyData data)
    {
        _context.Update(data);
        await _context.SaveChangesAsync();
        return true;
    }

    public async Task<CountryYearlyData?> FindAsync((string Country, int Year) key) =>
        await Collection.FirstOrDefaultAsync(d => (d.Country == key.Country || d.ISOCode == key.Country) && d.Year == key.Year);
}
