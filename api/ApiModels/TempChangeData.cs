using Microsoft.AspNetCore.Mvc.ModelBinding;
using Swashbuckle.AspNetCore.Annotations;

namespace RUG.WebEng.Api.ApiModels;
public class TemperatureChangeData
{
    public required string Country { get; set; }
    public int Year { get; set; }
    public double TemperatureChangeFromCH4 { get; set; }
    public double TemperatureChangeFromN2O { get; set; }
    public double TemperatureChangeFromCO2 { get; set; }
    public double ShareOfTemperatureChangeFromGHG { get; set; }


    public static TemperatureChangeData FromDatabase(Models.Emissions data) =>
        new ()
        {
            Country = data.Country,
            Year = data.Year,
            TemperatureChangeFromCH4 = data.TemperatureChangeFromCH4 ?? 0,
            TemperatureChangeFromN2O = data.TemperatureChangeFromN2O ?? 0,
            TemperatureChangeFromCO2 = data.TemperatureChangeFromCO2 ?? 0,
            ShareOfTemperatureChangeFromGHG = data.ShareOfTemperatureChangeFromGHG ?? 0,
        };
}