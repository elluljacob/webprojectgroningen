using Microsoft.AspNetCore.Mvc.ModelBinding;
using Swashbuckle.AspNetCore.Annotations;

namespace RUG.WebEng.Api.ApiModels;
public class EnergyData
{
    public required string Country { get; set; }
    public int Year { get; set; }
    public double EnergyPerCapita { get; set; }
    public double EnergyPerGDP { get; set; }

    public static EnergyData FromDatabase(Models.CountryYearlyData data) =>
        new()
        {
            Country = data.Country,
            Year = data.Year,
            EnergyPerCapita = data.EnergyPerCapita ?? 0.0,
            EnergyPerGDP = data.EnergyPerGDP ?? 0.0
        };
}
