using Microsoft.AspNetCore.Mvc.ModelBinding;
using Swashbuckle.AspNetCore.Annotations;

namespace RUG.WebEng.Api.ApiModels;

public class CountryYearlyData
{
    public required string Country { get; set; }
    public required string ISOCode { get; set; }
    public int Year { get; set; }
    public double GDP { get; set; }
    public double Population { get; set; }

    public static CountryYearlyData FromDatabase(Models.CountryYearlyData data) =>
        new()
        {
            Country = data.Country,
            ISOCode = data.ISOCode,
            Year = data.Year,
            GDP = data.GDP ?? 0.0,
            Population = data.Population ?? 0.0,
        };
}
