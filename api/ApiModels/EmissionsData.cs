using Microsoft.AspNetCore.Mvc.ModelBinding;
using Swashbuckle.AspNetCore.Annotations;

namespace RUG.WebEng.Api.ApiModels;

public class EmissionsData
{
    public required string Country { get; set; }
    public required string ISOCode { get; set; }
    public int Year { get; set; }
    public double CO2 { get; set; }
    public double Methane { get; set; }
    public double NitrousOxide { get; set; }
    public double TotalGHG { get; set; } 

    public static EmissionsData FromDatabase(Models.Emissions data) =>
        new()
        {
            Country = data.Country,
            ISOCode = data.ISOCode,
            Year = data.Year,
            CO2 = data.CO2 ?? 0.0,
            Methane = data.Methane ?? 0.0,
            NitrousOxide = data.NitrousOxide ?? 0.0,
            TotalGHG = data.TotalGHG ?? 0.0
        };
}

