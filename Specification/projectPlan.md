# Project Plan

## Deliverables

- **D2**: Deadline - 6th December
- **D3**: Deadline - 13th December
- **D4**: Deadline - 10th January

## Technology Stack

### Frontend
- JavaScript
- React.js

### Backend
- C#
- .NET

### Database
- MariaDB

