# How to Start and use the Application

1: Ensure docker is running and installed on your machine (assumed to be linux)  
2: Copy Paste the env.example file into your .env file (or do some minor changes whatever works best. I wouldn't recommend this though due to the structure of the databaseProvider, it only works if the dotnet port is 3001, would have improved it but didn't have time and had little to no help)  
3: Run "docker compose up" in the root of the project (where the docker-compose.yml file is located)  
4: Navigate to localhost:{FRONTEND_REACT_PORT} (in case of .env.example it's 3009)  

## Other note
The full report is available at /report/report.md
Also sometimes the api will start before the database and the connection will terminate on my machine.
So sometimes just run docker compose up again and it should correctly set up.
