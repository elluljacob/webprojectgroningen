
# Web Engineering 2023-24
Author: Jacob Eull (S5775299), Luffy Chau, Alejandro Penalba S5740541
January 2024


## Index {#index}

1.  [Introduction](#intro)

2.  [Requirements](#req)

3.  [Technologies](#tech)

    -   [API -- Back-End](#api)

    -   [Front-End](#frontend)

4.  [Requirements Evaluation](#reqeval)

5.  [RESTful API Maturity](#restful)

6.  [Generative AI](#ai)

7.  [Obstacles](#obstacles)

8.  [Distribution of work](#work)

9.  [Appendix](#appendix)

## Introduction {#intro}

For this project for the Web Engineering course, the task was to create
a web application in groups of two to three people. Our group consists
of Jacob Eull, Luffy Chau, and Alejandro Penalba.

The goal of this project is to demonstrate our ability to develop and
design a modern web application. It also aims to showcase a practical
understanding of the fundamental web technologies taught in the lectures
and tutorials of the course. For this web application, we leverage the
dataset collected and maintained by Our World in Data: CO2 and
Greenhouse Gas Emissions. This dataset includes metrics on CO2 emissions
(annual, per capita, cumulative, and consumption-based), other
greenhouse gases, energy combination, and many other relevant data.
However, in our application, only a reduced set of these metrics is
used, as specified in the requirements.

In this report, we will delve into the project requirements, the
technologies used for the development and design of the application, and
the complexities and obstacles encountered during the process.

## Requirements {#req}

In this section, we will explain in detail the multiple mandatory
requirements of the application.

#### Design and Document a RESTful API (REQ1) {#design-and-document-a-restful-api-req1}

The project requires the creation and documentation of a RESTful API.
This involves defining and documenting well-structured different
endpoints. The required functionalities are as follows:

-   To retrieve, create, update, or delete the population and GDP data
    for a country identified by its name or ISO code for a given year;

-   To retrieve all available emissions related data for a country
    identified by its name or ISO code, optionally filtered from a
    provided year and later;

-   To retrieve all temperature change data per continent, optionally
    filtered from a provided year and later;

-   To retrieve the energy per capita and per GDP data for all countries
    in a given year, if available, sorted per population size and
    returned in batches;

-   To retrieve the top or bottom countries based on their share of
    contribution to climate change either for a specific year or for the
    previous years.

#### JSON and CSV Representations (REQ2) {#json-and-csv-representations-req2}

The API endpoints must support data representations in both JSON and CSV
formats, although JSON will be the default.

#### Documentation of the API (REQ3) {#documentation-of-the-api-req3}

Create the necessary documentation for a user to use the API. This
should cover all endpoints and briefly explain the functionality each
endpoint covers, how to access them, possible API responses, and data
representation formats. The documentation should be easy to navigate
through a web browser.

#### Implementation of the API (REQ4) {#implementation-of-the-api-req4}

It is mentioned that optionally, we can use a database for back-end
persistence and are asked to implement the API as the back-end of the
app using any programming language or technology of our choice.

#### Implementation of Front-End (REQ5) {#implementation-of-front-end-req5}

The front-end should provide the user with access to all functionalities
indicated in REQ1. Although the UI should facilitate interaction with
the API, graphic design and the user interface will not be evaluated.

#### Docker and Deployment (REQ6) {#docker-and-deployment-req6}

It is a mandatory requirement to deliver a docker-compose.yml file to
facilitate the implementation of the entire application with the
\"docker compose up\" command.

#### Additional Documentation (REQ7) {#additional-documentation-req7}

Additional documentation is requested to complement the API
documentation, including a brief report detailing the chosen
technologies and our reasoning behind these choices, an assessment of
requirement compliance, the maturity of the RESTful API, identification
of potential issues, and the distribution of work among group members.

#### Generative AI Adoption (REQ8) {#generative-ai-adoption-req8}

In case any generative AI is used, it is required to detail the name,
version, and clarification regarding the use of the language model,
especially if it involves payment.

## Technologies {#tech}

Now we will explain which technologies we have used to develop and
implement the application.

### API -- Back-End {#api}

For the back-end, we used the C# programming language and the .NET Core
framework. This decision was clear and quick because from the beginning,
we knew that one of the subject tutorials would cover these
technologies. Even though some of us had never worked with this
programming language, we had all worked with similar languages, and it
seemed the most suitable for this project. We also know that these are
technologies with a large community and active support from Microsoft,
so we wouldn't have problems finding documentation or solutions to
potential issues along the way. However, the decision for the database
was not as swift. Well, initially, we quickly chose MongoDB for not
being SQL and for being easy to use. But after a few days, we decided to
switch to SQL since it was used in the tutorials, and Jacob had
enough experience to handle the design and implementation of the
database himself.

### Front-End {#frontend}

For the front-end, we used JavaScript and React. Like the back-end,
these decisions were influenced by the fact that, thanks to the large
community and class tutorials, we could find a lot of documentation, and
we wouldn't have problems finding solutions to errors that others have
encountered online.

## Requirements Evaluation {#reqeval}

In this section, we will evaluate how our application fulfills the
mandatory project requirements.

#### REQ1 {#req1}

The first requirement, designing and documenting a RESTful API, was
fulfilled by following the OpenAPI specification. The project includes a
file named \"spec.yml\" in the \"Specification\" folder which serves as the documentation 
and design of the api. The API was tested to properly fulfill all the requirements in 
a few ways. The first was using dotnet and running the api and using swagger as the frontend
to test everything. The second was using docker compose up and testing it using the frontend 
developed later in the project. The third was while the application is running using docker
using postman to ensure all requests made to the api are proper and functioning as expected when
the application is dockerized as postman provides more information about the data being returned
than our frontend.

#### REQ2 {#req2}

This requirement states that each endpoint should support JSON and CSV 
representations of the resources. This requirement is fulfilled as you 
can see we used a custom support class to convert most controllers 
ouptput to CSV format and return it as such. This was ensured by testing
each endpoint in postman while docker was running and asking for accept: 
text/csv and application/json using postman.


#### REQ3 {#req3}

This requirement is closely related to the first. The documentation of
the API should include all the necessary information for any user to
understand how an endpoint works, what it does, what responses to
expect, errors, etc. In Appendix 1, there is an example of our API
documentation. As can be seen, this requirement is also fulfilled.

#### REQ4 {#req4}

The fourth requirement is essential not only for the project, but also
for the application itself to function. Implementing the API as the
back-end of the web application. To make the implementation, we used the
.Net Core framework and the C# programming language. We also used a
database, as mentioned in the previous section on the technology used by
our app. Our back-end works just fine, so we believe this requirement is
also fulfilled.

#### REQ5 {#req5}

We believe this requirement is also fulfilled; we have implemented a front-end that
provides access to all API endpoints. Fortunately, user interface design
skills are not evaluated, we are not great designers, but we feel we have
fulfilled this requirement.

#### REQ6 {#req6}

Our program includes the required docker-compose.yml file and a \".env.example\"
with a default configuration to start the application simply by running
the \"docker compose up\" command in the root of the repository.

#### REQ7 {#req7}

The penultimate requirement asks us to include all the necessary
documentation in the project. On one hand, a brief project report (this
one), and on the other hand, sufficient code comments for understanding.
In our opinion, the code is clear and contains enough comments for
comprehension. For the project report, we are asked to have a section in
the report where we discuss the technologies we have chosen for the
application, another section where we comment on how our project meets
the mandatory requirements, a section on the maturity of the RESTful
API, and finally, a section to explain how we distributed the work, our
roles, and our performance. We understand this document as a report and
believe we have met this requirement.

#### REQ8 {#req8}

If we have used any type of help from a Generative AI (such as ChatGPT
or Copilot) we must document this help. As we have taken advantage of
this type of aid, in another section of this report we have added all
types of required documentation.

## RESTful API Maturity {#restful}

In this section, we will evaluate the maturity level of our API, based
on the following levels:

1.  Plain Old XML

2.  Resource Identification

3.  Uniform Interface

4.  HATEOAS: Hypermedia as the Engine of Application State

In our opinion, our API is at the second level, Uniform Interface. Now
we will explain why we believe that our API meets each level or not:

-   Level 0: An API at level 0 means that it uses XML as a data exchange
    format. Our API exceeds this level by using JSON as the default
    format and also accepting CSV for data exchange, improving
    flexibility and readability.

-   Level 1: An API at this level, must define each resource with a URI
    and must use HTTP operations to interact with these resources. Each
    resource in our API is correctly identified by a unique URI, and
    HTTP operations are used to interact with these resources.

-   Level 2: An API at level 2 tells us that an API uses HTTP verbs
    correctly, that is, it uses GET to read resources, POST to create
    new ones, PATCH to update them, and DELETE to delete them. Our API
    follows these semantic standards, therefore we understand that this
    level also exceeds it.

-   Level 3: Being at this level means being able to access resources
    from one, that is, a resource can contain links to related ones. Our
    API does not have these links between resources, which is why we
    believe that our API is at level 2.

## Generative AI {#ai}

Currently, AI is a very valuable tool. As a group, we believe that not
using these kinds of tools would unnecessarily complicate our work.
While they may not provide the perfect solution to our problems, they
help guide us or give us a path to follow. That's why we've used them.

For this project, we've taken advantage of both OpenAI's LLM: ChatGPT
and Microsoft's tool: Github Copilot. In the case of ChatGPT, we
accessed it through a web browser, utilizing its free version, GPT-3.5.
In the [appendix](#chatgpt-chats), we have provided links to the chats we had with the tool. These chats include inquiries about errors during programming,
where, in some cases, we simply provided a method we had created to get
feedback on potential errors or bugs. Additionally, I used ChatGPT to
translate texts. Being from Spain and having Spanish as my native
language, I wrote some parts of the report in Spanish and then used
ChatGPT to translate them into English, considering it offers a more
accurate translation than other translators.

I used Github Copilot in Visual Studio Code. Although Github Copilot is
a paid tool, I have been able to access it for free with my student
github account. Documenting everything Copilot has done or helped with
is challenging because this tool uses a logic of code as a prompt. We
observed its significant impact in speeding up code creation, suggesting
code blocks based on the comments and code snippets we had already made.
This made us faster in project development. We have attached in the [appendix](#github-copilot-suggested-code-blocks) screenshots
with some text blocks suggested by Copilot, which were largely used by
us.

## Obstacles {#obstacles}

The initial obstacles we encountered in developing the application
included having to learn new programming languages while understanding
how the required technologies worked. This was something we knew we had
to face from the very beginning. For instance, I (Alejandro) had not dealt with
databases or web page design before, so everything related to the
front-end or initializing the database was new to me. I also had no
prior experience with C#. I (Jacob) had dealt with databases, dockerizing and 
some minimal frontend design in plain old javascript.   

A significant obstacle was dockerizing the application. Most of the
group members had not worked with Docker before, and it proved to be quite
challenging for us. Another great challenge with this project in our case
was the distribution of work, more on this later.

In the end, we realized that some parts of the back-end weren't working
quite well, especially when we requested resources in the form of CSV.
However, after debugging and rectifying some lines of code, we fixed it.

## Distribution of work {#work}

For the first delivery, we discussed the tech stack and deadlines all together through WhatsApp and Discord.
For the second delivery, we had to design the API, and we divided the endpoints of REQ1 as follows:
1 and 2: Alejandro
3: Luffy
4 and 5: Jacob

Regarding the implementation of the back-end, Jacob took care of the database, ApiModels, Repositories, part of the Models directory (DBSeeder and DBContext). Alejandro handled the controllers, and Luffy took care of the models. However, in the end, everyone contributed a bit to each other's tasks as we helped each other fix errors.
As for the front-end, Jacob guided us and told us what to do since he was the most knowledgeable. Alejandro worked on the API part, Luffy did a page, Jacob handled everything else. Jacob and Alejandro then were responsable for debugging and fixing/improving code.

Anything related to Docker and CI/CD ended up being Jacob's responsibility, although we all tried to learn and collaborate.
In my case (Jacob) a large amount of the code was taken from the gitlab tutorials repository. This code was either
copy pasted (will be marked with a comment within the file), or modified and changed, or used as an example on how
to structure and code my own classes and files.

Alejandro was in charge of the report. 

## Appendix {#appendix}

#### 1. Link to the project description {#link-to-the-project-description}

[Web Engineering 2023-24 Project Description](https://brightspace.rug.nl/d2l/le/lessons/243208/topics/3663624)

#### 2. Link to the repository with the web app {#link-to-the-repository-with-the-web-app}

[GitLab Group 51](https://gitlab.com/rug-cs/courses/web-engineering/2023-2024/students/group-51/group-51-project)

#### 3. Generative AI {#generative-ai}

#### 3.1 ChatGPT chats {#chatgpt-chats}

1.  [Resolve \"@emotion/react\" Import](https://chat.openai.com/share/32d09855-d1a6-4143-ac97-933a21a5e33e)

2.  [Docker Testing: C# TypeScript](https://chat.openai.com/share/e9a9e6a9-1e3d-4a8c-a349-2c923b0137da)

3.  [RESTful API Maturity Levels](https://chat.openai.com/share/8c1a00b0-d44b-455c-96d4-f57478dcdbdb)

4.  [Spanish-English Translation Assistance](https://chat.openai.com/share/967afdb2-f43d-457c-870c-88d1debe9eb0)

5. [Creating a starting point for the DBSeeder](https://chat.openai.com/share/e127d6b1-66d8-4e1f-a4b0-2c0aa3cf97c4)

6. [Getting the dataset to be in json and csv](https://chat.openai.com/share/61ac36f4-b6f2-4262-8344-b3bbf9f8e112)

7. [Help creating the pages and more for the frontend](https://chat.openai.com/share/091d6d8b-a483-48c3-864e-6954e6976df4)

8. [Enhance ClimateChange api](https://chat.openai.com/share/d074e42c-1fa7-4845-a36a-548b95b59945)

9. [Understanding react and getting started with the frontend](https://chat.openai.com/share/23e4aa76-cd4c-48e4-b1c4-b67dc2da549b)

10. [Fixing docker and the DBSeeder](https://chat.openai.com/share/1308427c-d21f-4c63-8d87-2de46d7ddca8)

11. [Creating the CSV converters](https://chat.openai.com/share/f7db569e-702b-4bb3-af56-58f158ec0a69)

12. [Creating CI/CD pipeline](https://chat.openai.com/share/1ab351ab-5913-4757-914a-4e681e2d358d)

13. [Confirming query on CI/CD.yml file](https://chat.openai.com/share/ea6086d6-99bc-4463-ba42-51d43a078a1f)

14. [How to check container log history](https://chat.openai.com/share/c24e434f-b57d-4ee4-be3f-a648fac9ce97)


#### 3.2 GitHub Copilot suggested code blocks {#github-copilot-suggested-code-blocks}

![image](images/Untitled4.png)

![image](images/Untitled.png)

![image](images/Untitled3.png)

![image](images/Untitled2.png)

![image](images/Untitled5.png)

#### 3.3 Comments on the use of AI - Jacob
I mostly used AI to serve as a starting point and to construct large blocks of code for me already. Once this code
was written, then I would iterate over it over the span of a day or multiple days and improve areas that were throwing errors or could be drastically improved design wise and efficiency wise. Links 5-14 are mine and I
mostly used the paid version of ChatGPT (4.0 as the time of this report). I also used it for debugging, and 
verifying that my code seemed right. My use of GPT was mostly due to the unfamiliarity of the subject and the 
vast amount of code I hade to write as later on (in the frontend), I took the responsabilities of another teammate.
I feel as though the use of GPT for this project was justified for my reasons and it absolutely did help me especially in getting used to the languages and concepts used throughout the project.